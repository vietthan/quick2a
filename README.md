# Amichay Test 2

Table of Contents
=================

* [Clone, Configure, and Run the Two Apps](#clone-configure-and-run-the-two-apps)
    * [Clone](#clone)
    * [Configure and Run the app on port 6543](#configure-and-run-the-app-on-port-6543)
    * [Configure and Run the app on port 6544](#configure-and-run-the-app-on-port-6544)
    * [Result](#result)
* [Start and load Redis](#start-and-load-redis)
* [Run OpenResty and curl results](#run-openresty-and-curl-results)
* [Postman Testing](#postman-testing)
* [PyTests](#pytests)
---
## Clone, Configure, and Run the Two Apps
It would be best to follow the steps strictly.
### Clone
Move to desired location then run:

```bash
$ git clone git@bitbucket.org:vietthan/quick2a.git
```

### Configure and Run the app on port 6543
Assuming installed `python3`. Run:

```bash
$ cd quick2/quick_tutorial2
$ export VENV=$(pwd)/env
$ python3 -m venv $VENV
$ cd ini
$ $VENV/bin/pip install -e .
$ $VENV/bin/pserve development.ini --reload &
```

### Configure and Run the app on port 6544
Assuming installed `python3`. Run:

```bash
$ cd ../../quick_tutorial3
$ export VENV=$(pwd)/env
$ python3 -m venv $VENV
$ cd ini
$ $VENV/bin/pip install -e .
$ $VENV/bin/pserve development.ini --reload &
```

### Result

Run in Chrome:

![hello world from 2 apps](run2apps.png)

or `curl` it:

![hello world from 2 apps with curl](curl2apps.png)

---
## Start and load Redis
Assuming redis is installed.

Start redis in the background:

```bash
$ redis-server --daemonize yes
```

Add keys to store host and ports:

```bash
$ redis-cli set port1 127.0.0.1:6543   
$ redis-cli set port2 127.0.0.1:6544
```  

## Run OpenResty and curl results
Assumed OpenResty is installed, check to make sure the OpenResty version of nginx is used with:

```bash
which nginx
```

Hopefully it points to the nginx executable that accompanies OpenResty.

return to the root of the project and run nginx on the accompanied file:

```bash
$ nginx -c $(pwd)/nginx-redis.conf 
```

Use `curl` to confirm results:

```bash
$ curl --header "X-VIET: port1" localhost:80
$ curl --header "X-VIET: port2" localhost:80
```

You should get:

![supply the xviet header](xviet2apps.png)

---
## Postman Testing

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/18769888-bb114d32-c23f-485e-8c3b-3fa8dc1dcd0a?action=collection%2Ffork&collection-url=entityId%3D18769888-bb114d32-c23f-485e-8c3b-3fa8dc1dcd0a%26entityType%3Dcollection%26workspaceId%3D00bb2e01-23cb-4a09-8ade-c054ec7c907a)

![postman test of xviet](postman2apps.png)

## PyTests

A few tests were written in `tests/` at root of project. The tests can be run with:

```bash
$ pytest -rx -v -s tests/
```

## How to shutdown everything

Presumably all instances are ran from the same shell:

```bash
$ redis-cli shutdown
$ nginx -s quit
$ fg
^C  # Ctrl+C, send SIGINT to 2nd pserve (quick-tutorial3)
$ fg
^C  # Ctrl+C, send SIGINT to 1st pserve (quick-tutorial2)
```
