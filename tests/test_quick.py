import requests

def test_6543():
    url = "http://localhost:6543"
    response = requests.request("GET", url)
    assert response.status_code == 200
    assert b'<h1>6543: Hello World!</h1>' in response.content

def test_6544():
    url = "http://localhost:6544"
    response = requests.request("GET", url)
    assert response.status_code == 200
    assert b'<h1>6544: Hello World!</h1>' in response.content


def test_local_400_status():
    url = "http://localhost:80"
    response = requests.request("GET", url)
    assert response.status_code == 400


def test_port_keys(redis_cache):
    exit_code = redis_cache.ping()
    assert exit_code == True
    
    url = "http://localhost:80"
    headers = {'X-VIET': 'port1'}
    exit_code = redis_cache.set('port1', '127.0.0.1:6543')
    assert exit_code == True

    response = requests.request("GET", url, headers=headers)
    assert response.headers['X-VIET'] == '127.0.0.1:6543'
    assert b'<h1>6543: Hello World!</h1>' in response.content
    
    headers["X-VIET"] = 'port2'
    exit_code = redis_cache.set('port2', '127.0.0.1:6544')
    assert exit_code == True

    response = requests.request("GET", url, headers=headers)
    assert response.headers['X-VIET'] == '127.0.0.1:6544'
    assert b'<h1>6544: Hello World!</h1>' in response.content

def test_set_routes(redis_cache):
    exit_code = redis_cache.ping()
    assert exit_code == True
        
    url = "http://localhost:80"
    headers = {'X-VIET': 'route'}
    exit_code = redis_cache.set('route', 'a')
    assert exit_code == True

    response = requests.request("GET", url, headers=headers)
    assert response.headers['X-VIET'] == 'a'
    assert b'<h1>6543: Hello World!</h1>' in response.content

    exit_code = redis_cache.set('route', 'b')
    assert exit_code == True

    response = requests.request("GET", url, headers=headers)
    assert response.headers['X-VIET'] == 'b'
    assert b'<h1>6544: Hello World!</h1>' in response.content