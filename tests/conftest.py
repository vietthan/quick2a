import pytest
import redis

@pytest.fixture(scope="session")
def redis_cache():
    return redis.Redis(host='localhost', port=6379, db=0)
    